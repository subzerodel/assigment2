package com.company;

import java.util.ArrayList;
import java.util.List;

public class Developer implements IEmployee {
    private String name;
    private List<Developer> developers;

    public Developer(String name) {
        this.name = name;
        developers = new ArrayList<>();
    }

    @Override
    public boolean work() {
        System.out.println(Developer.class + " is doing own job");
        for (Developer developer : developers) {
            developer.work();
        }
        return true;
    }

    public boolean createDeveloper() {
        System.out.println("Developer created");
        return true;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Developer> getDevelopers() {
        return developers;
    }

    public void setDevelopers(List<Developer> developers) {
        this.developers = developers;
    }

    public void addDeveloper (Developer developer) {
        developers.add(developer);
    }
}
