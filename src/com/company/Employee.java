package com.company;

import java.util.ArrayList;
import java.util.List;

public class Employee implements IEmployee{
    public String getResponsibility() {
        return responsibility;
    }

    public void setResponsibility(String responsibility) {
        this.responsibility = responsibility;
    }

    public String responsibility="none";
    public String name;
    public List<Employee> coworkers;
    public Employee(String name){
        this.name=name;
        this.responsibility=responsibility;
        coworkers=new ArrayList<>();

    }

    @Override
    public String toString() {
        return "name " + name
                + " position " + responsibility;
    }

    @Override
    public boolean work() {
        System.out.println(Employee.class+" is working");
        for (Employee coworker: coworkers) {
            coworker.work();
        }
        return true;
    }

    public boolean createEmployee(){
        System.out.println("employee created");
        return true;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Employee> getCoworkers() {
        return coworkers;
    }

    public void setCoworkers(List<Employee> coworkers) {
        this.coworkers = coworkers;
    }

    public void addCoworker(Employee coworker){
        coworkers.add(coworker);
    }
}



