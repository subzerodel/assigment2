package com.company;

public class ProxyEmployee implements IEmployee,IProxy {
    public Employee Employee;
    public Manager manager;

    public ProxyEmployee(String name, Manager manager){
        this.manager = manager;
        Employee = new Employee(name);
    }

    public boolean haveProjectManager(){
        return manager != null;
    }

    @Override
    public boolean work(){
        if (haveProjectManager()) {
            return Employee.work();
        }
        return false;
    }

    @Override
    public void addWorker(IProxy worker) {
        if (worker instanceof ProxyEmployee) Employee.addCoworker(((ProxyEmployee) worker).Employee);
    }

    public boolean createBackEnd(){
        if (haveProjectManager())
            return Employee.createEmployee();
        return false;
    }

}
