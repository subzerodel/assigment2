package com.company;

public class DeveloperProxy implements IProxy, IEmployee {
    public Developer developer;
    public Manager manager;

    public DeveloperProxy(String name, Manager manager) {
        this.manager = manager;
        developer = new Developer(name);
    }

    public boolean haveManager() {
        return manager != null;
    }

    @Override
    public boolean work() {
        if (haveManager()) {
            return developer.work();
        }
        return false;
    }

    @Override
    public void addWorker(IProxy employee) {
        if (employee instanceof DeveloperProxy) developer.addDeveloper(((DeveloperProxy) employee).developer);
    }

    public boolean createDeveloper() {
        if (haveManager())
            return developer.createDeveloper();
        return false;
    }

}
