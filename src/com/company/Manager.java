package com.company;

public class Manager implements IEmployee {
    String name;
    public Manager(String name){
        this.name = name;
    }

    @Override
    public boolean work() {
        System.out.println(Manager.class + "is working");
        return true;
    }

    public boolean finishWork(boolean backEnd, boolean frontEnd){
        return backEnd && frontEnd;
    }

}
